import React, {Component} from 'react';
// import Loadable from 'react-loadable';
import loadable from 'loadable-components';
import './App.css';
import Home from './containers/Home/Home';
import ProductDetail from './containers/ProductDetail/ProductDetail';
import AddressList from './containers/AddressList/AddressList';
// import AddressConfig from './containers/AddressConfig/AddressConfig';
import {Route, Switch} from 'react-router-dom';
import Success from './components/ExchangeSuccess/ExchangeSuccess';
// import ExchangeConfirm from './containers/ExchangeConfirm/ExchangeConfirm';

const LoadingComponent = () => <div>Loading...</div>;
const LoadableComponent = loadable(() =>
  import(/* webpackChunkName: "exchangeConfirm" */'./containers/ExchangeConfirm/ExchangeConfirm'), {
  LoadingComponent
});
const AddressConfigOnDemand = loadable(() =>
  import(/* webpackChunkName: "addressconfig" */'./containers/AddressConfig/AddressConfig'), {
  LoadingComponent
});

class App extends Component {
  state = {
    addresses: []
  };

  componentDidMount() {
    setTitle("积分商城");
  }

  handleEditAddress = id => {
    this.setState(prevState => ({
      addresses: prevState.addresses.map(addr => addr)
    }))
  };

  handleDeleteAddress = id => {
    this.setState(prevState => ({
      addresses: prevState.addresses.filter(addr => addr.id !== id)
    }));
  };

  handleSetAsDefault = id => {
    this.setState(prevState => ({
      addresses: prevState.addresses.map(addr => ({
        ...addr,
        isDefault: addr.id !== id ? false : true
      }))
    }))
  };
  handleSelect = id => alert(id);

  render() {
    const test = (
      <Switch>
        <Route path="/" exact component={Home}/>
        <Route path="/productDetail" component={ProductDetail}/>
        <Route path="/exchangeConfirm" component={LoadableComponent}/>
        <Route path="/addressList" render={(props) => (
          <AddressList {...props} addresses={this.state.addresses}
                       onSelect={this.handleSelect}
                       onEdit={this.handleEditAddress}
                       onDelete={this.handleDeleteAddress}
                       onSetAsDefault={this.handleSetAsDefault}
                       onAddAddress={this.handleAddAddress}/>
        )}/>
        <Route path="/addressConfig" component={AddressConfigOnDemand}/>
        <Route path="/success" component={Success}/>
      </Switch>
    );
    return (
      test
    );
  }
}

export default App;
