import React from 'react';
import PropTypes from 'prop-types';
import CSSModules from 'react-css-modules';
import classes from './notice.scss';

const Notice = ({ title, content, onAcknowledge }) => {
  return (
    <div styleName="notice">
      <h2 styleName="title">
        {title}
      </h2>
      <p styleName="content" dangerouslySetInnerHTML={{
        __html: content
      }} />
      <div styleName="footer" onClick={onAcknowledge}>
        <span>知道了</span>
      </div>
    </div>
  );
};

Notice.propTypes = {};
Notice.defaultProps = {};

export default CSSModules(Notice, classes);
