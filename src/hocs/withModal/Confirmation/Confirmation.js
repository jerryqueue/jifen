import React from 'react';
import PropTypes from 'prop-types';
import CSSModules from 'react-css-modules';
import classes from './confirmation.scss';

const Confirmation = ({ content, onCancel, onConfirm, cancelText, confirmText }) => {
  return (
    <div styleName="confirmation">
      <h2 styleName="title">
        提示信息
      </h2>
      <p styleName="content" dangerouslySetInnerHTML={{
        __html: content
      }} />
      <div styleName="footer">
        <div styleName="btn" onClick={onCancel}>
          <span>{ cancelText }</span>
        </div>
        <div styleName="btn" onClick={onConfirm}>
          <span>{ confirmText }</span>
        </div>
      </div>
    </div>
  );
};

Confirmation.propTypes = {};
Confirmation.defaultProps = {};

export default CSSModules(Confirmation, classes);
