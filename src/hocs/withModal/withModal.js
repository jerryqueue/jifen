import React from 'react';
import CSSModules from 'react-css-modules';
import classes from './modal.scss';
import Notice from './Notice/Notice';
import Confirmation from './Confirmation/Confirmation';

export default (Component) => {
  @CSSModules(classes)
  class WithModal extends React.Component {
    state = {
      type: 'notice',
      modalVisible: false,
      title: '',
      content: ""
    };
    showNotice = ({title, content}) => {
      this.setState({
        type: 'notice',
        modalVisible: true,
        title,
        content
      });
    };
    hideDialog = () => {
      this.setState({modalVisible: false});
    };
    showConfirmation = ({
                          content,
                          onConfirm,
                          onCancel,
                          cancelText,
                          confirmText
                        }) => {

      this.cancelText = cancelText || '取消';
      this.confirmText = confirmText || '确定';
      // Store callbacks in the instance
      this.confirmed = () => {
        this.hideDialog();
        return onConfirm && onConfirm();
      };
      this.canceled = () => {
        this.hideDialog();
        return onCancel && onCancel();
      };
      this.setState({
        type: 'confirmation',
        modalVisible: true,
        content
      });
    };

    render() {
      const {type, modalVisible, title, content} = this.state;

      const dialogs = {
        notice: <Notice title={title} content={content} onAcknowledge={this.hideDialog}/>,
        confirmation: <Confirmation content={content}
                                    cancelText={this.cancelText}
                                    confirmText={this.confirmText}
                                    onCancel={this.canceled}
                                    onConfirm={this.confirmed}/>
      };
      return (
        <div>
          <Component {...this.props}
                     showNotice={this.showNotice}
                     hideNotice={this.hideDialog}
                     showConfirmation={this.showConfirmation}/>
          {
            modalVisible ? (
              <div styleName="modal">
                {dialogs[type]}
              </div>
            ) : null
          }
        </div>
      )
    }
  }

  return WithModal;
};