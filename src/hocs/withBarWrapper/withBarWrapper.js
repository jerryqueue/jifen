import React from 'react';
import styles from './bar-wrapper.scss';
import CSSModules from 'react-css-modules';
import PropTypes from 'prop-types';

const WithBarWrapper
  = ({
       showInnerBottomBorder,
       innerBottomBorderWidth = '1px',
       innerBottomBorderColor = 'black',
       style,
       ...props
     }) => {
  return (
    <div style={{ ...style, paddingBottom: 0 }}>
      <div style={{
        paddingBottom: style.paddingBottom || 0,
        borderBottomWidth: showInnerBottomBorder ? innerBottomBorderWidth : 0,
        borderBottomStyle: 'solid',
        borderBottomColor: innerBottomBorderColor
      }}>
        {props.children}
      </div>
    </div>
  );
};

WithBarWrapper.propTypes = {};
WithBarWrapper.defaultProps = {};



export default (WrappedComponent, styleProps) => {
  const BarWrapperWithCSSModules = CSSModules(WithBarWrapper, styles);

  return ({ showInnerBottomBorder, ...props }) => {
    let revisedStyles = styleProps;

    if (showInnerBottomBorder != null) {
      revisedStyles = {
        ...styleProps,
        showInnerBottomBorder: showInnerBottomBorder
      };
    }
    return (
      <BarWrapperWithCSSModules {...revisedStyles}>
        <WrappedComponent {...props} />
      </BarWrapperWithCSSModules>
    );
  }
};
