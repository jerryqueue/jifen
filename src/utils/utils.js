import { Toast } from 'antd-mobile';
import 'style-loader!css-loader!antd-mobile/dist/antd-mobile.min.css';

export function stemProperties(source, schema) {
  if (!Array.isArray(schema)) throw new Error("Schema must be an array.");
  let result = {};
  schema.forEach(sc => {
    if (typeof sc === 'string') result[sc] = source[sc];
    else {
      result = {
        ...result,
        ...fromSchemaObject(sc)
      };
    }
  });

  return result;

  function fromSchemaObject(schemaObj) {
    return Object.keys(schemaObj).reduce((acc, originalKey) => {
      const rule = schemaObj[originalKey];
      const type = typeof rule;
      const originalValue = source[originalKey];

      if (type === 'string') {

        acc[rule] = originalValue;

      } else if (type === 'function') {

        const field = rule(originalValue, originalKey, source);
        if (!field) throw new Error('A rule must return an object describing the revamped field');
        if (!field.key) throw new Error('The object returned from the rule function must contain a key prop.');
        acc[field.key] = field.value;

      } else {
        throw new Error('Schema expects only String or Function as conversion rules, but got type ' + type);
      }
      return acc;
    }, {});
  }
}

export function validate(rules, o) {
  return Object.keys(rules).every(key => {
    let target = o[key];
    if (typeof target.trim === 'function') {
      target = target.trim();
    }

    if (typeof rules[key] === 'function') {
      return rules[key](target);
    } else { // 进行空值校验
      if (target !== 0 && !target) {
        Toast.info(rules[key]);
        return false;
      }
      return true
    }
  });
}