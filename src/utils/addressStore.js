import addressJson from './addresses';

export let addresses = [];

export default {
  replaceAll: (addrs) => addresses = addrs,
  getAddressInUse: () => {
    let result = addresses.find(addr => addr.inUse);
    if (!result) {
      result = addresses.find(addr => addr.defaultUnit === 'Y');
    }
    if (!result && addresses.length > 0) {
      result = addresses[0];
    }
    return result;
  },
  setAddressInUse: id => {
    addresses = addresses.map(addr => ({
      ...addr,
      inUse: addr.id == id
    }));
  },
  add: addr => {
    if (addr.defaultUnit === 'Y') {
      addresses.forEach(addr => addr.defaultUnit = 'N');
    }
    addresses.push(addr);
  },
  update: addr => {
    addresses = addresses.map(a => {
      const isDefault = addr.defaultUnit === 'Y';
      if (a.id != addr.id) { // 如果待更新的地址是默认地址，需要将其他地址设为非默认地址
        return !isDefault ? a : {...a, defaultUnit: 'N'};
      }
      return addr;
    });
  },
  remove: id => {
    addresses = addresses.filter(addr => addr.id !== id);
  },
  getAddressDescription: addr => {
    const province = addressJson.find(p => p.provinceCode === addr.provinceCode);
    if (!province) return {
      provinceName: '',
      cityName: '',
      countyName: ''
    };
    const city = province.cities.find(c => c.cityCode === addr.cityCode);
    const county = city && city.counties.find(ct => ct.countyCode === addr.countyCode);
    return {
      provinceName: province.provinceName,
      cityName: city && city.cityName || '',
      countyName: county && county.countyName || ''
    };
  }
}
