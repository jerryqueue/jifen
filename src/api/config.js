import axios from 'axios';
import { Toast } from 'antd-mobile';
import env from '../../env';

let baseURL = '/appapi/za-plutuscat';
console.log("process.env.NODE_ENV", process.env.NODE_ENV);
if (process.env.NODE_ENV === 'production') {
  baseURL = env.baseURL;
}
const newInstance = axios.create({
  baseURL,
  timeout: 45000,
  responseType: 'json',
  withCredentials: true
});

// Add a response interceptor
newInstance.interceptors.response.use(function (response) {
  // Do something with response data
  const data = response.data;
  if (!data || !data.success) {
    console.log('发生错误', response);
    const errorMsg = (data && data.msg) || '未知错误';
    throw errorMsg;
  }
  return response;
}, function (error) {
  return Promise.reject(error);
});

newInstance.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

export default newInstance;