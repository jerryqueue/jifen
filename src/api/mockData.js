export const products = [
  {
    activityId: 10196001,
    activityType: 0,
    imageUrl: "//staticdaily.zhongan.com/website/store/online/upload/10132001-mobile-1522310790930-1.jpeg",
    openUrl: "/open/exg/exgDetail?activityId=10196001",
    point: 0,
    primeBenefit: 1,
    primePoint: 50,
    status: 2,
    suitLevel: 2,
    title: "谷铭积分测试活动"
  },
  {
    activityId: 10194007,
    activityType: 0,
    imageUrl: "//staticdaily.zhongan.com/website/store/online/upload/10131003-mobile-1521097238591-1.jpeg",
    openUrl: "/open/exg/exgDetail?activityId=10194007",
    point: 0,
    primeBenefit: 0,
    primePoint: 50,
    status: 2,
    suitLevel: 1,
    title: "发现爱奇艺会员月卡"
  },
  {
    activityId: 10194006,
    activityType: 0,
    imageUrl: "//staticdaily.zhongan.com/website/store/online/upload/10131004-mobile-1521097504085-1.jpeg",
    openUrl: "/open/exg/exgDetail?activityId=10194006",
    point: 0,
    primeBenefit: 0,
    primePoint: 50,
    status: 2,
    suitLevel: 1,
    title: "发现抽奖活动--5元话费"
  },
  {
    activityId: 10194004,
    activityType: 0,
    imageUrl: "//staticdaily.zhongan.com/website/store/online/upload/10131002-mobile-1521096379269-1.jpeg",
    openUrl: "/open/exg/exgDetail?activityId=10194004",
    point: 0,
    primeBenefit: 0,
    status: 2,
    title: "飞利浦恒温电吹风"
  },
  {
    activityId: 10194003,
    activityType: 0,
    imageUrl: "//staticdaily.zhongan.com/website/store/online/upload/10131001-mobile-1521096191344-1.jpeg",
    openUrl: "/open/exg/exgDetail?activityId=10194003",
    point: 0,
    primeBenefit: 1,
    primePoint: 50,
    status: 2,
    title: "众安纪念T恤"
  },
  {
    activityId: 10191005,
    activityType: 0,
    imageUrl: "//staticdaily.zhongan.com/website/store/online/upload/10123005-mobile-1512528119213-1.jpeg",
    openUrl: "/open/exg/exgDetail?activityId=10191005",
    point: 0,
    primeBenefit: 0,
    primePoint: 50,
    status: 2,
    suitLevel: 1,
    title: "决明子甜梦枕头"
  },
  {
    activityId: 10191004,
    activityType: 0,
    imageUrl: "//staticdaily.zhongan.com/website/store/online/upload/10123004-mobile-1512527852020-1.jpeg",
    openUrl: "/open/exg/exgDetail?activityId=10191004",
    point: 10560,
    primeBenefit: 0,
    status: 2,
    title: "威戈WENGER休闲运动双肩背包"
  }
];