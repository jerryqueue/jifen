import React from 'react';
import CSSModules from 'react-css-modules';
import classes from './address-summary.scss';
import PropTypes from 'prop-types';
import location from '../../assets/images/location.png';
import rightArrow from '../../assets/images/rightArrow.png';
import { withRouter } from 'react-router-dom';
import addressHandler from '../../utils/addressStore';

const AddressSummary = ({ address, history }) => {
  const desc = addressHandler.getAddressDescription(address);
  return (
    <div styleName="address-summary" onClick={() => history.push('/addressList')}>
      <div styleName="left">
        <img src={location} alt="loc."/>
      </div>
      <div styleName="middle">
        <div styleName="name-phone">
          <span>{address.userName} {address.phone}</span>
        </div>
        <div styleName="addr">
          {desc.provinceName} {desc.cityName} {desc.countyName} {address.address}
        </div>
      </div>
      <div styleName="right">
        <img src={rightArrow} alt="arrow"/>
      </div>
    </div>
  );
};

AddressSummary.propTypes = {};
AddressSummary.defaultProps = {};

export default withRouter(CSSModules(AddressSummary, classes));
