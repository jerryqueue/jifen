import React from 'react';
import PropTypes from 'prop-types';
import CSSModules from 'react-css-modules';
import styles from './product-item.scss';
import viptag from '../../assets/images/viptag.png';

const ProductItem = ({ info, onItemClick }) => (
    <div styleName="product-item" onClick={() => onItemClick(info)}>
      <div styleName="img-section">
        <img src={info.imageUrl} alt=""/>
      </div>
      <div styleName="desc">
        <span>{info.title}</span>
      </div>
      <div styleName="jifen">
        <span>{info.point}积分</span>
        {
          info.primeBenefit == 1 ? (
            <div styleName="special">
              <img src={viptag} alt="vip" />
              <span>{info.primePoint}积分</span>
            </div>
          ): null
        }
      </div>
    </div>
);

ProductItem.propTypes = {};
ProductItem.defaultProps = {};

export default CSSModules(ProductItem, styles);
