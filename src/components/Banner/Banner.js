import React from 'react';
import CSSModules from 'react-css-modules';
import PropTypes from 'prop-types';
import styles from './banner.scss';
import questionMark from '../../assets/images/questionmark.png';
import { oldBase } from "../../utils/constants";

const Banner = ({ points, level, times, onGotoOrders}) => {
  let levelClass = `v${level}`;
  if (levelClass !== 'v1'
    && levelClass !== 'v2'
    && levelClass !== 'v3') {
    levelClass = 'v1';
  }
  return (
    <div styleName={`banner ${levelClass}`}>
      <div styleName="normal">
        <span>我的积分</span>
      </div>
      <div styleName="amount" onClick={() => location.href=`${oldBase}/point/pointDetailNew`}>
        <span>{points}</span>
        <span styleName="normal">积分</span>
        <div styleName="triangle"></div>
      </div>
      <div styleName="bottom">
        <div styleName="left" onClick={() => location.href='https://static.zhongan.com/website/assets/zaapp/dm-other/discover/assets/demo/index.html'}>
          <span>V{level}会员：本月特价 {times} 次</span>
          <img src={questionMark} alt=""/>
        </div>
      </div>
      <div styleName="myorder" onClick={onGotoOrders}>
        <span>我的订单</span>
        <div styleName="triangle"></div>
      </div>
    </div>
  );
};

Banner.propTypes = {};
Banner.defaultProps = {};

export default CSSModules(Banner, styles, { allowMultiple: true });
