import React from 'react';
import styles from './amount-chooser.scss';
import CSSModules from 'react-css-modules';
import PropTypes from 'prop-types';
import bag from '../../assets/images/bag.png';
import Counter from '../UI/Counter/Counter';

const AmountChooser = ({
    amount, onAmountChange, product
  }) => {
  const { unitPrice, desc, imgSrc } = product;

  return (
    <div styleName="amount-chooser">
      <div styleName="preview">
        <div styleName="image">
          <img src={imgSrc} alt="bag"/>
        </div>
        <div styleName="detail">
          <div styleName="title">
            <span>{ desc }</span>
          </div>
          <div styleName="jifen">
            <span styleName="num">{ unitPrice }</span>
            <span styleName="jifenText">积分</span>
          </div>
        </div>
      </div>
      <div styleName="dividor"></div>
      <div styleName="amount-handler">
        <div styleName="amountText">
          <span>数量</span>
        </div>
        <div>
          <Counter value={amount} onChange={direc => onAmountChange(direc)} />
        </div>
      </div>
    </div>
  );
};

AmountChooser.propTypes = {};
AmountChooser.defaultProps = {};

export default CSSModules(AmountChooser, styles);
