import React from 'react';
import classes from './contact.scss';
import CSSModules from 'react-css-modules';
import PropTypes from 'prop-types';
import AddAddressEntry from '../AddAddressEntry/AddAddressEntry';
import AddressSummary from '../AddressSummary/AddressSummary';
import AddressCard from '../AddressCard/AddressCard';
import addressHandler from '../../utils/addressStore';

const Contact = (props) => {
  const addressToUse = addressHandler.getAddressInUse();
  const ui =
    addressToUse
    ? <AddressSummary address={addressToUse}/>
    : <AddAddressEntry/>;

  return (
    <div styleName="contact">
      { ui }
    </div>
  );
};

Contact.propTypes = {};
Contact.defaultProps = {};

export default CSSModules(Contact, classes);
