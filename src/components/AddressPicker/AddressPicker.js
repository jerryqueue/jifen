import React from 'react';
import PropTypes from 'prop-types';
import CommonPicker from '../VUE/CommonPicker.vue';
import {VueInReact} from 'vuera';

const Component = VueInReact(CommonPicker);

const AddressPicker = ({ visible, onConfirm }) => {
  return (
    <div>
      <Component visible={visible}
                 onConfirm={onConfirm}
                 onDismiss={() => {}}/>
    </div>
  );
};

AddressPicker.propTypes = {};
AddressPicker.defaultProps = {};

export default AddressPicker;
