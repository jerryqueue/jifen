import React from 'react';
import CSSModules from 'react-css-modules';
import classes from './addaddressentry.scss';
import PropTypes from 'prop-types';
import plus from '../../assets/images/plus.png';
import rightArrow from '../../assets/images/rightArrow.png';
import { withRouter } from 'react-router-dom';

const AddAddressEntry = ({ history }) => {
  return (
    <div styleName="addaddressentry" onClick={() => history.push('/addressConfig')}>
      <div styleName="left">
        <img src={plus} alt="plus"/>
        <span>添加收货地址</span>
      </div>
      <div styleName="right">
        <img src={rightArrow} alt=""/>
      </div>
    </div>
  );
};

AddAddressEntry.propTypes = {};
AddAddressEntry.defaultProps = {};

export default withRouter(CSSModules(AddAddressEntry, classes));
