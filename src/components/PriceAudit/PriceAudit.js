import React from 'react';
import PropTypes from 'prop-types';
import CSSModules from 'react-css-modules';
import styles from './price-audit.scss';
import DeductionBar from '../DeductionBar/DeductionBar';
import BarWrapper from '../DeductionBar/DeductionBarWrapper/DeductionBarWrapper';

const PriceAudit = (props) => {
  return (
    <div styleName="price-audit">
      <DeductionBar {...props} />
      <BarWrapper>
        <div styleName="total">
          <div styleName="left">合计</div>
          <div styleName="right">
            <span styleName="jifen">{props.totalPrice}</span>
            <span>积分</span>
          </div>
        </div>
      </BarWrapper>
    </div>
  );
};

PriceAudit.propTypes = {};
PriceAudit.defaultProps = {};

export default CSSModules(PriceAudit, styles);
