import React from 'react';
import PropTypes from 'prop-types';
import classes from './exchange-success.scss';
import CSSModules from 'react-css-modules';
import successMark from '../../assets/images/success.png';
import Button from '../UI/Button/Button';
import {oldBase} from "../../utils/constants";
import withModal from '../../hocs/withModal/withModal';

const title = '订单处理时效说明';
const content = "兑换的虚拟物品于兑换后10个工作日内发放至用户账户（手机号码）；<br> 实物礼品于兑换后15个工作日内发放至用户预留地址，如遇节假日则顺延。";
const ExchangeSuccess = ({history, showNotice}) => {
  return (
    <div styleName="success">
      <div styleName="container">
        <div>
          <img src={successMark} alt="success"/>
        </div>

        <div styleName="success-text">
          <span>兑换成功</span>
        </div>
        <p styleName="grayText">我们会尽快处理您的订单，请耐心等候</p>
        <p styleName="blueText" onClick={() => showNotice({title, content})}>订单处理时效说明</p>
        <div styleName="button-section">
          <div styleName="btn">
            <Button style={styles.returnBtn} onClick={() => history.replace('/')}>返回商场</Button>
          </div>
          <div styleName="btn">
            <Button style={styles.viewBtn} onClick={() => location.href = `${oldBase}/draw/cjorders`}>查看订单</Button>
          </div>
        </div>
      </div>
    </div>
  );
};

const styles = {
  returnBtn: {
    border: '1px solid #12c286',
    color: '#12c286',
    display: 'flex !important',
    alignItems: 'center',
    marginRight: '.28rem'
  },
  viewBtn: {
    background: '#12c286',
    color: 'white',
    display: 'flex'
  }
};
ExchangeSuccess.propTypes = {};
ExchangeSuccess.defaultProps = {};

export default withModal(CSSModules(ExchangeSuccess, classes));
