import React from 'react';
import CSSModules from 'react-css-modules';
import styles from './price-detail.scss';
import PropTypes from 'prop-types';

const PriceDetail = ({ product }) => {
  return (
    <div styleName="price-detail">
      <div styleName="title">
        <span>{ product.title }</span>
      </div>
      <div styleName="prices">
        <div styleName="normal">
          <span styleName="desc">普通价</span>
          <div styleName="jifen">
            <span styleName="num">{ product.normalPrice }</span>
            <span styleName="jifenText">积分</span>
          </div>
        </div>
        <div styleName="vertical-bar"></div>
        <div styleName="vip">
          <span styleName="desc">会员价</span>
          <div styleName="jifen">
            <span styleName="num">{ product.vipPrice }</span>
            <span styleName="jifenText">积分</span>
          </div>
          <div styleName="extra-text">
            <span>{ product.extraText }</span>
          </div>
        </div>
      </div>
    </div>
  );
};

PriceDetail.propTypes = {};
PriceDetail.defaultProps = {};

export default CSSModules(PriceDetail, styles);
