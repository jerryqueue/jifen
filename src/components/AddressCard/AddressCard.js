import React from 'react';
import CSSModules from 'react-css-modules';
import classes from './address-card.scss';
import PropTypes from 'prop-types';
import graycircle from '../../assets/images/graycircle.png';
import checked from '../../assets/images/checked.png';
import pencil from '../../assets/images/pencil.png';
import deleteImg from '../../assets/images/delete@3x.png';
import IconedText from '../UI/IconedText/IconedText';
import addressHandler from '../../utils/addressStore';

const AddressCard = (props) => {
  const {
    addressInfo = {},
    onSetAsDefault,
    onEdit,
    onDelete,
    onSelect
  } = props;
  const {
    userName: name,
    phone,
    address,
    defaultUnit
  } = addressInfo;

  const {
    provinceName,
    cityName,
    countyName
  } = addressHandler.getAddressDescription(addressInfo);

  return (
    <div styleName="address-card">
      <div styleName="top" onClick={onSelect}>
        <h4>{name} {phone}</h4>
        <p>{provinceName} {cityName} {countyName} {address}</p>
      </div>
      <div styleName="bottom">
        <div styleName="left" onClick={onSetAsDefault}>
          <img src={defaultUnit === 'Y' ? checked : graycircle} alt="checked"/>
          <span>默认地址</span>
        </div>
        <div styleName="right">
          <div styleName="iconed-text" onClick={onEdit}>
            <IconedText imgSrc={pencil} text="编辑"/>
          </div>
          <div styleName="iconed-text" onClick={onDelete}>
            <IconedText imgSrc={deleteImg} text="删除"/>
          </div>
        </div>
      </div>
    </div>
  );
};

AddressCard.propTypes = {};
AddressCard.defaultProps = {};

export default CSSModules(AddressCard, classes);
