import React from 'react';
import styles from './custom-input.scss';
import CSSModules from 'react-css-modules';
import PropTypes from 'prop-types';
import mobile from '../../assets/images/mobile.png';
import withBarWrapper from '../../hocs/withBarWrapper/withBarWrapper';

const CustomInput =
  ({
     imageStyle,
     inputHeight,
     inputFontSize,
     inputType,
     onInputChange,
     value
   }) => {
    return (
      <div styleName="custom-input">
        <div styleName="image" style={imageStyle}>
          <img src={mobile} alt="mobile"/>
          {/*<span>手机号</span>*/}
        </div>
        <div styleName="input">
          <input type={inputType}
                 value={value}
                 onChange={onInputChange}
                 placeholder="输入手机号码"
                 style={{
                   height: inputHeight + 'rem',
                   fontSize: inputFontSize + 'rem',
                   paddingTop: (inputHeight - inputFontSize) / 2 + 'rem',
                   paddingBottom: (inputHeight - inputFontSize) / 2 + 'rem',
                 }}/>
        </div>
      </div>
    );
  };

CustomInput.propTypes = {};
CustomInput.defaultProps = {};

export default withBarWrapper(
  CSSModules(CustomInput, styles),
  {
    style: {
      backgroundColor: 'white',
      paddingTop: '.35rem',
      paddingBottom: '.35rem',
      paddingLeft: '.3rem',
      paddingRight: '.3rem'
    },
    showInnerBottomBorder: false,
  }
);
