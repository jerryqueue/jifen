import React from 'react';
import CSSModules from 'react-css-modules';
import styles from './button.scss';
import PropTypes from 'prop-types';

const Button = ({ style, ...props }) => {
  return (
    <div styleName="button"
         style={style} onClick={() => props.onClick()}>
        <div styleName="buttonContentWrapper">
          { props.children }
        </div>
    </div>
  );
};

Button.propTypes = {};
Button.defaultProps = {};

export default CSSModules(Button, styles);
