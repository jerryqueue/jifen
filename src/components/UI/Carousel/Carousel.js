import React from 'react';
import {Carousel as ReactCarousel} from 'react-responsive-carousel';
// import 'style-loader!css-loader!react-responsive-carousel/lib/styles/main.min.css';
import 'style-loader!css-loader!react-responsive-carousel/lib/styles/carousel.min.css';

const Carousel = (props) => {
  return (
    <div>
      <ReactCarousel dynamicHeight
                     autoPlay
                     infiniteLoop
                     interval={3000}
                     showStatus={false}
                     swipeScrollTolerance={.1}
                     showThumbs={false} showArrows={false}
                     swipeable={false}>
        {
          props.images.map((img, i) => (
            <div key={i}>
              <img src={img} alt=""/>
            </div>
          ))
        }
      </ReactCarousel>
    </div>
  );
};

Carousel.propTypes = {};
Carousel.defaultProps = {};

export default Carousel;
