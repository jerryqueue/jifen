import React from 'react';
import styles from './counter.scss';
import CSSModules from 'react-css-modules';
import PropTypes from 'prop-types';

const Counter = ({ value, onChange }) => {
  return (
    <div styleName="counter">
      <div styleName="decreaser" onClick={() => onChange('-')}>
        <span>-</span>
      </div>
      <div styleName="input">
        <div styleName="num">
          <span>{value}</span>
        </div>
      </div>
      <div styleName="increaser" onClick={() => onChange('+')}>
        <span>+</span>
      </div>
    </div>
  );
};

Counter.propTypes = {};
Counter.defaultProps = {};

export default CSSModules(Counter, styles);
