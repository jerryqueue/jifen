import React from 'react';
import styles from './sticky-footer.scss';
import CSSModules from 'react-css-modules';
import PropTypes from 'prop-types';

const StickyFooter = (props) => {
  return (
    <div styleName="sticky-footer"
         style={{
           ...props.style,
           height: props.height / 100 + 'rem'
         }}>
      { props.children }
    </div>
  );
};

StickyFooter.propTypes = {};
StickyFooter.defaultProps = {};

export default CSSModules(StickyFooter, styles);
