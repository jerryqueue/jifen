import React, {Component} from 'react';
import PropTypes from 'prop-types';
import CSSModules from 'react-css-modules';
import classes from './tip.scss';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

@CSSModules(classes)
class Tip extends Component {
  state = {
    visible: false,
    msg: '',
  };

  show(msg, timeout = 2000) {
    this.setState({ visible: true, msg });
    setTimeout(() => this.hide(), timeout);
  }

  hide() {
    this.setState({ visible: false });
  }

  render() {
    return (
      <ReactCSSTransitionGroup
        transitionAppear={true}
        transitionAppearTimeout={500}
        transitionName="tip-content"
        transitionEnterTimeout={500}
        transitionLeaveTimeout={300}>
        {
          this.state.visible ? (
            <div styleName="tip">
              <div styleName="content">
                <span>{this.state.msg}</span>
              </div>
            </div>
          ) : null
        }
      </ReactCSSTransitionGroup>
    );
  }
}

Tip.propTypes = {};
Tip.defaultProps = {};

export default Tip;
