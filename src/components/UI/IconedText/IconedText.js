import React from 'react';
import PropTypes from 'prop-types';
import CSSModules from 'react-css-modules';
import classes from './iconed-text.scss';

const IconedText = ({imgSrc, text}) => {
  return (
    <div styleName="iconed-text">
      <img src={imgSrc} alt=""/>
      <span>{text}</span>
    </div>
  );
};

IconedText.propTypes = {};
IconedText.defaultProps = {};

export default CSSModules(IconedText, classes);
