import BarWrapper from './DeductionBarWrapper/DeductionBarWrapper';
import styles from './deduction-bar.scss';
import React from 'react';
import CSSModules from 'react-css-modules';
import PropTypes from 'prop-types';
import checked from '../../assets/images/checked.png';
import unchecked from '../../assets/images/circle.png';
import { withRouter } from 'react-router-dom';
import qs from 'qs';

@withRouter
@CSSModules(styles)
class DeductionBar extends React.Component {
  render() {
    const {
      pointsSavedPerProduct,
      times, // 会员价剩余可用次数
      actualTimes, // 当前所使用的会员价次数
      usePrevelige, // 是否使用会员折扣
      onCheckMarkClick
    } = this.props;

    const { showVipSection } = qs.parse(this.props.location.search.substring(1));
    let vipSection = null;
    if (showVipSection === 'true') {
      vipSection = (
        <BarWrapper showInnerBottomBorder={true}>
          <div styleName="container">
            <div styleName="text">
              <div styleName="left">
                <div>使用会员价折扣</div>
                <div styleName="lighter">
                  本月还剩 <span styleName="times">{times}</span> 次
                </div>
              </div>
              { usePrevelige ? (
                <div styleName="right">
                  <div>-{pointsSavedPerProduct * actualTimes} 积分</div>
                  <div styleName="lighter">-{pointsSavedPerProduct} x {actualTimes}</div>
                </div>
              ) : null }
            </div>
            <div styleName="checkmark" onClick={onCheckMarkClick}>
              <img src={usePrevelige ? checked : unchecked} alt=""/>
            </div>
          </div>
        </BarWrapper>
      );
    }
    return (
      <div styleName="deduction-bar">
        {vipSection}
      </div>
    );
  }
};

DeductionBar.propTypes = {};
DeductionBar.defaultProps = {};

export default DeductionBar;
