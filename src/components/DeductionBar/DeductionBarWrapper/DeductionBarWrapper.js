import React from 'react';
import withBarWrapper from '../../../hocs/withBarWrapper/withBarWrapper';

export default withBarWrapper(
  (props) => <div>{ props.children }</div>,
  {
    style: {
      backgroundColor: 'white',
      paddingTop: '.27rem',
      paddingBottom: '.27rem',
      paddingLeft: '.3rem',
      paddingRight: '.3rem'
    },
    innerBottomBorderColor: '#f2f2f2'
  }
);
