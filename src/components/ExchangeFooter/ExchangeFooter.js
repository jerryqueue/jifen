import React from 'react';
import styles from './exchange-footer.scss';
import CSSModules from 'react-css-modules';
import StickyFooter from '../UI/StickyFooter/StickyFooter';
import PropTypes from 'prop-types';

const ExchangeFooter = (props) => {
  const { enough } = props;
  let leftContent = "积分不足，赚点再来";
  if (props.enough) {
    leftContent = (
      <div>
        <span styleName="num">{ props.points }</span>
        <span styleName="normal">积分</span>
      </div>
    );
    if (props.qualified) {
      leftContent = (
        <div>
          <span styleName="num">{ props.primePoints }</span>
          <span styleName="normal">积分</span>
          <span styleName="points">{props.points}积分</span>
        </div>
      );
    }
  }
  return (
    <StickyFooter height={120}>
      <div styleName="exchange-footer">
        <div styleName="left">
          { leftContent }
        </div>
        <div styleName="right" onClick={props.onButtonClick}>
          <span>{ !enough ? '赚积分' : '兑换' }</span>
        </div>
      </div>
    </StickyFooter>
  );
};

ExchangeFooter.propTypes = {};
ExchangeFooter.defaultProps = {};

export default CSSModules(ExchangeFooter, styles);
