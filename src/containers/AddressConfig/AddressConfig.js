import React from 'react';
import classses from './add-address.scss';
import CSSModules from 'react-css-modules';
import AddressItemWrapper from './AddressItemWrapper/AddressItemWrapper';
import Button from '../../components/UI/Button/Button';
import axios from '../../api/config';
import addressHandler, { addresses } from '../../utils/addressStore';
import { validate } from "../../utils/utils";
import qs from 'qs';
import rightArrow from '../../assets/images/rightArrow.png';
import AddressPicker from '../../components/AddressPicker/AddressPicker';
import { Switch } from 'antd-mobile';
import Tip from '../../components/UI/Tip/Tip';

class AddressConfig extends React.Component {
  state = {
    userName: '',
    phone: '',
    zipCode: '',
    address: '',
    defaultUnit: 'Y',
    province: {},
    city: {},
    county: {},
    pickerVisible: 0 // 0: 不可见 >0: 可见
  };
  componentDidMount() {
    const addressInUrl = qs.parse(this.props.location.search.substring(1));
    this.id = addressInUrl.id;
    if (this.id) {
      document.title = '编辑地址';
      this.setState(addressInUrl);
    } else {
      document.title = '新增地址';
    }
  }
  async handleSave() {
    const addressInfo = this.state;
    if (validate({
      userName: '姓名不能为空',
      phone: '手机号不能为空',
      zipCode: '邮政编码不能为空',
      province: p => {
        if (!p.provinceCode) this.tip.show('所在地区不能为空');
        return !!p.provinceCode;
      },
      address: '详细地址不能为空',
    }, addressInfo)) {
      try {
        const isNew = !this.id;
        const params = { ...addressInfo };
        params.provinceCode = params.province.provinceCode;
        params.cityCode = params.city.cityCode;
        params.countryCode = params.county.countyCode;
        delete params.province;
        delete params.city;
        delete params.county;

        if (!isNew) params.id = this.id;

        loading.show();
        const queryString = '?' + qs.stringify(params);
        const response = await axios.get('/user-address-modify' + queryString);

        if (isNew) {
          addressHandler.add({
            ...params,
            id: response.data && response.data.obj
          });
          if (addresses.length === 1) { // 只有一个地址时，设置为当前使用的地址
            addressHandler.setAddressInUse(response.data.obj);
          }
        } else {
          addressHandler.update(params);
        }
        this.props.history.goBack();
      } catch(e) {
        console.error(e);
      } finally {
        loading.hide();
      }
    }
  };
  handleInputChange = (e, fieldName) => {
    this.setState({
      [fieldName]: e.target.value
    });
  };
  handleRegionSelect = (province, city, county) => {
    this.setState({ province, city, county });
  };
  toggleDefault = () => {
    this.setState(prevState => ({
      defaultUnit: prevState.defaultUnit === 'Y' ? 'N' : 'Y'
    }));
  };
  render() {
    const { provinceName: pname } = this.state.province;
    const { cityName: cname } = this.state.city;
    const { countyName: ctyname } = this.state.county;

    return (
      <div styleName="add-address">
        <AddressItemWrapper>
          <div styleName="input-line">
            <div styleName="label">
              <span>收货人姓名</span>
            </div>
            <input type="text" value={this.state.userName}
                   onChange={e => this.handleInputChange(e, 'userName')}
                   placeholder="请输入收货人的真实姓名"/>
          </div>
        </AddressItemWrapper>

        <AddressItemWrapper>
          <div styleName="input-line">
            <div styleName="label">
              <span>手机号码</span>
            </div>
            <input type="tel" value={this.state.phone}
                   onChange={e => this.handleInputChange(e, 'phone')}
                   placeholder="请输入收货人的手机号码"/>
          </div>
        </AddressItemWrapper>

        <AddressItemWrapper>
          <div styleName="input-line">
            <div styleName="label">
              <span>邮政编码</span>
            </div>
            <input type="text" value={this.state.zipCode}
                   onChange={e => this.handleInputChange(e, 'zipCode')}
                   placeholder="收货地址的邮政编码"/>
          </div>
        </AddressItemWrapper>

        <AddressItemWrapper>
          <div styleName="input-line">
            <div styleName="label">
              <span>所在地区</span>
            </div>
            <div styleName="region" onClick={() => this.setState({ pickerVisible: this.state.pickerVisible+1 })}>
              <div style={pname ? {color: '#464646'} : {}}>
                <span>{pname ? `${pname} ` : '省、' }</span>
                <span>{cname ? `${cname} ` : '市、' }</span>
                <span>{ctyname ? `${ctyname} ` : (!pname  ? '区' : '')}</span>
              </div>
              <img src={rightArrow} alt="arrow"/>
            </div>
          </div>
        </AddressItemWrapper>

        <AddressItemWrapper showInnerBottomBorder={false}>
          <div styleName="input-line">
            <div styleName="label">
              <span>详细地址</span>
            </div>
            <input type="text" value={this.state.address}
                   onChange={e => this.handleInputChange(e, 'address')}
                   placeholder="请填写详细地址，不少于5个字"/>
          </div>
        </AddressItemWrapper>

        <div styleName="setDefault">
          <AddressItemWrapper showInnerBottomBorder={false}>
            <div styleName="input-line">
              <div styleName="label">
                <span>设为默认地址</span>
              </div>
              {/*<Switch checked={this.state.defaultUnit === 'Y'} onChange={this.toggleDefault} />*/}
              <Switch checked={this.state.defaultUnit === 'Y'} onChange={this.toggleDefault} />
            </div>
          </AddressItemWrapper>
        </div>

        <Button style={styles.button} onClick={() => this.handleSave()}>保 存</Button>
        <AddressPicker visible={this.state.pickerVisible} onConfirm={this.handleRegionSelect}/>
        <Tip ref={tip => this.tip = tip} />
      </div>
    );
  }
};

const styles = {
  button: {
    color: 'white',
    backgroundColor: '#12c286',
    width: '6.68rem'
  }
};
AddressConfig.propTypes = {};
AddressConfig.defaultProps = {};

export default CSSModules(AddressConfig, classses);
