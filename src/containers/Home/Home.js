import React, {Component} from 'react';
import PropTypes from 'prop-types';
import CSSModules from 'react-css-modules';
import styles from './home.scss';
import Banner from '../../components/Banner/Banner';
import ProductList from '../ProductList/ProductList';
import Button from '../../components/UI/Button/Button';
import axios from '../../api/config';
import {stemProperties} from "../../utils/utils";
import qs from 'qs';
import {oldBase} from "../../utils/constants";
import { updateTotalPoints } from "../../utils/sharedData";

@CSSModules(styles)
class Home extends Component {
  state = {
    points: '', // 积分数
    level: 0, // 会员等级 V1, V2, V3
    times: 0, // 特价剩余次数
    products: [] // 商品列表
  };

  async queryAccount() {
    try {
      const response = await axios.get('/point-user');
      const data = response.data.obj;
      const newState = stemProperties(data, ['level', {
        point: 'points',
        leftCount: 'times'
      }]);
      this.setState(newState, () => updateTotalPoints(this.state.points));
    } catch (e) {
      console.error(e);
    }
  };

  onViewMore = () => {
    location.href = oldBase + '/open/exg/exgList.htm?limit=100';
  };
  async querySelectedProducts() {
    try {
      const params = {
        channel: 1024,
        type: 0,
        start: 0,
        limit: 2000
      };
      const response = await axios.get('/point-goods?' + qs.stringify(params));
      const data = response.data.rows;
      this.setState({products: data});
    } catch (e) {
      console.error(e);
    }
  }

  async componentDidMount() {
    loading.show();
    try {
      await Promise.all([
        this.queryAccount(),
        this.querySelectedProducts()
      ]);
    } finally {
       loading.hide();
    }
  }

  render() {
    return (
      <div styleName="home">
        <Banner {...this.state} onGotoOrders={() => location.href = `${oldBase}/draw/cjorders`}/>
        <ProductList items={this.state.products}/>
        <Button style={localStyles.button} onClick={this.onViewMore}>查看更多</Button>
      </div>
    );
  }
}

const localStyles = {
  button: {
    display: 'flex',
    color: '#12c287',
    background: 'white',
    border: '1px solid #12c287'
  },
};
Home.propTypes = {};
Home.defaultProps = {};

export default Home;
