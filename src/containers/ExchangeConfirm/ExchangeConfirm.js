import React, {Component} from 'react';
import CSSModules from 'react-css-modules';
import classes from './exchange-confirm.scss';
import CustomInput from '../../components/CustomInput/CustomInput';
import AmountChooser from '../../components/AmountChooser/AmountChooser';
import PriceAudit from '../../components/PriceAudit/PriceAudit';
import StickyFooter from '../../components/UI/StickyFooter/StickyFooter';
import Button from '../../components/UI/Button/Button';
import Contact from '../../components/Contact/Contact';
import qs from 'qs';
import axios from '../../api/config';
import addressHandler, { addresses as addressesInStore } from '../../utils/addressStore';
import Tip from '../../components/UI/Tip/Tip';
import { totalPoints, updateTotalPoints } from "../../utils/sharedData";

@CSSModules(classes)
class ExchangeConfirm extends Component {
  state = {
    imgageUrl: '',
    usePrevelige: true,  // 是否使用会员折扣价
    amount: 1,  // 已选的商品数量
    mobile: '', // 手机号
    title: '',
    point: '',
    primePoint: '',
    leftCount: 0
  };

  async componentDidMount() {
    const productInfo = qs.parse(this.props.location.search.substring(1));
    this.setState({...productInfo, mobile: productInfo.userPhone});
    this.activityId = productInfo.activityId;
    this.goodsType = productInfo.goodsType;
    if (productInfo.goodsType == 1 && !addressesInStore.length) { // 实物商品
      try {
        loading.show();
        const response = await axios.get('/user-address');
        const addresses = response.data.rows;

        // 后端把 countyCode 错拼成 countryCode
        addressHandler.replaceAll(addresses.map(addr => ({...addr, countyCode: addr.countryCode })));

        this.forceUpdate();
      } catch (e) {
        console.error('didmount', e);
      } finally {
        loading.hide();
      }
    }
  }

  handleAmountChange = (direction) => {
    const factor = direction === '+' ? 1 : -1;
    this.setState(prevState => {
      let newAmount = prevState.amount + factor;
      if (newAmount <= 0) newAmount = 1;
      console.log('-------------------------------------')
      console.log('getTotalPrice before if', this.getTotalPrice(newAmount));
      console.log('totalPoints before if', totalPoints);
      if (this.getTotalPrice(newAmount) > totalPoints && direction === '+') {
        console.log('getTotalPrice', this.getTotalPrice(newAmount));
        console.log('totalPoints', totalPoints);
        console.log('---------------------------------------')
        this.tip.show("积分不够");
        return;
      }
      return { amount: newAmount };
    });
  };
  handleMobileChange = (e) => {
    this.setState({
      mobile: e.target.value
    });
  };
  toggleUsePrevelige = () => {
    this.setState(prevState => ({
      usePrevelige: !prevState.usePrevelige
    }));
  };
  getActualTimes = (amount) => {
    const {usePrevelige} = this.state;
    const {leftCount = 0} = this.state;
    if (!usePrevelige) return 0; // 如果未使用会员价格，则会员价格使用次数为0
    return amount > leftCount ? leftCount : amount;
  };
  getTotalPrice = (amount) => {
    amount = amount || this.state.amount;
    const { point, primePoint } = this.state;
    const totalPrice = amount * point - this.getActualTimes(amount) * (point - primePoint);

    console.log('--------------------');
    console.log('amount', amount);
    console.log('point', point);
    console.log('primePoint', primePoint);
    console.log('totalPoint', totalPrice);
    console.log('-----------------------');
    return totalPrice;
  };
  async handleExchange() {
    try {
      // 如果所需积分大于用户帐户总积分，提示积分不够
      if (this.getTotalPrice(this.state.amount) > totalPoints) {
        this.tip.show("积分不够");
        return;
      }

      const params = {
        activityId: this.activityId,
        buyNumber: this.state.amount,
        phoneNum: this.state.mobile,
        discountNum: this.getActualTimes(this.state.amount)
      };
      if (this.goodsType == 1) {
        const addressInUse = addressHandler.getAddressInUse();
        if (!addressInUse) {
          this.tip.show("请先添加收货地址");
          return;
        }
        params.addressId = addressInUse.id;
      }
      if (!this.state.mobile.trim()) {
        this.tip.show("请输入联系电话");
        return;
      }
      loading.show();
      const { data: result } = await axios.get('/point-goods-exg?' + qs.stringify(params));

      if (result.success) {
        updateTotalPoints(totalPoints - this.getTotalPrice());
        this.props.history.replace('/success');
      }

    } catch(e) {
      console.error(e);
      this.tip.show(e);
    } finally {
      loading.hide();
    }
  };
  render() {
    const {
      title, // 商品名称描述
      point, // 正常积分价
      primePoint, // 会员积分价
      leftCount, // 剩余会员价次数
      imageUrl
    } = this.state;

    // 实际使用会员价的次数
    const actualTimes = this.getActualTimes(this.state.amount);
    const product = { // 当前产品信息
      desc: title,
      unitPrice: point,
      imgSrc: imageUrl
    };
    return (
      <div styleName="exchange-confirm">
        <div styleName="tip">
          <span>注：下单成功后不可取消</span>
        </div>
        {
          this.state.goodsType == 0 ? (
            <div styleName="input-section">
              <CustomInput imageStyle={styles.input}
                           value={this.state.mobile}
                           onInputChange={this.handleMobileChange}
                           inputFontSize={0.3} inputType="tel"/>
            </div>
          ) : <Contact />
        }
        <div style={styles.chooser}>
          <AmountChooser amount={this.state.amount}
                         product={product}
                         onAmountChange={this.handleAmountChange}/>
        </div>

        <PriceAudit pointsSavedPerProduct={point - primePoint}
                    usePrevelige={this.state.usePrevelige}
                    times={leftCount} actualTimes={actualTimes}
                    onCheckMarkClick={this.toggleUsePrevelige}
                    totalPrice={this.getTotalPrice()}/>

        <StickyFooter height={120}>
          <div styleName="btn">
            <div style={{width: '100%'}}>
              <Button style={styles.button} onClick={() => this.handleExchange()}>确认兑换</Button>
            </div>
          </div>
        </StickyFooter>

        <Tip ref={tip => this.tip = tip} />
      </div>
    );
  }
}

const styles = {
  button: {
    display: 'flex',
    color: 'white',
    background: '#12c287'
  },
  chooser: {
    marginBottom: '.15rem'
  },
  input: {
    width: '.47rem',
    marginRight: '.28rem'
  }
};
ExchangeConfirm.propTypes = {};
ExchangeConfirm.defaultProps = {};

export default ExchangeConfirm;
