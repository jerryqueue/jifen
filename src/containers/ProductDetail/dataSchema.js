export default [
  'title',
  'imgCoverList',
  'point',
  'primeBenefit',
  'primePoint',
  'leftCount',
  'shortPoint',
  'userPhone',
  'imgDetailList',
  'imageUrl',
  'goodsType',
  'level'
];