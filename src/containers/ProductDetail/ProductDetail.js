import React from 'react';
import PropTypes from 'prop-types';
import Carousel from '../../components/UI/Carousel/Carousel';
import CSSModules from 'react-css-modules';
import classes from './product-detail.scss';
import viptag from '../../assets/images/viptag.png';
import question from '../../assets/images/question.png';
import ExchangeFooter from '../../components/ExchangeFooter/ExchangeFooter';
import axios from '../../api/config';
import { stemProperties } from "../../utils/utils";
import qs from 'qs';
import dataSchema from './dataSchema';
import withModal from '../../hocs/withModal/withModal';

@withModal
@CSSModules(classes)
class ProductDetail extends React.Component {
  state = {
    imgCoverList: [null], // 产品主介绍图 // I don't know why but there has to be something in this array or carousel won't work.
    title: '',
    point: '',        // 正常积分价
    primePoint: '',   // 会员积分价
    primeBenefit: 1, // '1'——会员商品；'0'——普通商品
    leftCount: 0,        // 当用户为会员时显示会员剩余优惠次数
    shortPoint: true,  // 积分是否足够购买
    userPhone: '', // 用户手机号
    imgDetailList: [],      // 商品详情图
    level: 0,          // 会员等级
    imageUrl: '',      // 产品单张主图,
    goodsType: 1,     // 产品类型 1——实物商品；0——虚拟商品
  };

  async getProductDetail() {
    try {
      const addr = `/point-goods-detail${this.props.location.search}`;
      const response = await axios.get(addr);

      const data = stemProperties({...response.data.attributes, ...response.data.obj}, dataSchema);
      this.setState(data);
    } catch(e) {
      console.error(e);
    }
  }
  async componentDidMount() {
    try {
      loading.show();
      await this.getProductDetail();
    } finally {
      loading.hide();
    }
  }
  showNotice = () => {
    if (this.state.level == 1) {
      this.props.showConfirmation({
        content: '完成任务提升会员等级，即可获得优惠兑换特权！',
        cancelText: '下次再说',
        confirmText: '去看看',
        onConfirm: () => location.href = 'zaapp://zai.discover?'
      });
    } else {
      this.props.showNotice({
        title: '提示',
        content: '不同等级会员每月可享受的会员专享价购买数量不同，会员等级越高，可购买数量越多哦!'
      });
    }
  };
  getVipBar() {
    const { primeBenefit, primePoint,
      level, leftCount } = this.state;

    if (primeBenefit == 0) return null; // 该商品不具有会员价优惠
    return (
      <div styleName="vipbar">
        <div styleName="left">
          <img src={viptag} alt="vip"/>
          <span>会员价 {primePoint} 积分</span>
        </div>
        <div styleName="right" onClick={this.showNotice}>
          {
            level == 1
              ? <span>如何获取会员价</span>
              : <span>本月还可兑换 {leftCount} 次</span>
          }
          <img src={question} alt=""/>
        </div>
      </div>
    );
  }
  handleButtonClick = () => {
    const { level, primeBenefit, leftCount } = this.state;
    const productInfo = stemProperties(this.state, [
      'title', 'imageUrl', 'point', 'primePoint', 'primeBenefit',
      'leftCount', 'goodsType', 'userPhone'
    ]);
    if (!this.state.shortPoint) { // 积分足够
      const queryString = `${this.props.location.search}&` + qs.stringify({
        ...productInfo,
        showVipSection: level != 1 && primeBenefit == 1 && leftCount > 0 // 会员等级大于1，且是会员价商品
      });
      this.props.history.push('/exchangeConfirm' + queryString);
    } else { // 积分不足
      window.location.href = 'zaapp://zai.VIP.AllTasks?';
    }
  };
  render() {
    const { imgCoverList, shortPoint, primeBenefit, level,
      point, primePoint, leftCount, imgDetailList } = this.state;
    return (
      <div styleName="product-detail">
        <div styleName="top">
          <div styleName="banner">
            {
              imgCoverList.length === 1
                ? <img src={imgCoverList[0]} styleName="singleImg" alt=""/>
                : <Carousel images={imgCoverList}/>
            }
          </div>
          <div styleName="detail">
            <h3>{this.state.title}</h3>
            <p>
              <span styleName="jifenValue">{this.state.point}</span>
              <span styleName="jifenText">积分</span>
            </p>
            { this.getVipBar() }
          </div>
        </div>
        <div styleName="bottom">
          <div styleName="detailText">
            <span styleName="line"></span>
            <span>商品详情</span>
            <span styleName="line"></span>
          </div>
          {
            imgDetailList.map((src, i) => <img src={src} alt="" key={i}/>)
          }
        </div>

        <ExchangeFooter points={point} enough={!shortPoint}
                        primePoints={primePoint}
                        onButtonClick={this.handleButtonClick}
                        qualified={ level != 1 && primeBenefit == 1 && leftCount > 0 } />
      </div>
    );
  }
}

ProductDetail.propTypes = {};
ProductDetail.defaultProps = {};

export default ProductDetail;
