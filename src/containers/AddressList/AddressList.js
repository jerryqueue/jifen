import React from 'react';
import classes from './address-list.scss';
import CSSModules from 'react-css-modules';
import PropTypes from 'prop-types';
import AddressCard from '../../components/AddressCard/AddressCard';
import StickyFooter from '../../components/UI/StickyFooter/StickyFooter';
import Button from '../../components/UI/Button/Button';
import addressHandler, { addresses } from '../../utils/addressStore';
import qs from 'qs';
import axios from '../../api/config';
import Tip from '../../components/UI/Tip/Tip';
import withModal from '../../hocs/withModal/withModal';

@withModal
@CSSModules(classes)
class AddressList extends React.Component {
  addAddress = () => {
    this.props.history.push('/addressConfig');
  };
  handleSelect = id => {
    addressHandler.setAddressInUse(id);
    this.props.history.goBack();
  };
  handleEdit = addr => {
    const desc = addressHandler.getAddressDescription(addr);

    this.props.history.push('/addressConfig?' + qs.stringify({
      ...addr,
      province: {
        provinceName: desc.provinceName,
        provinceCode: addr.provinceCode
      },
      city: {
        cityName: desc.cityName,
        cityCode: addr.cityCode
      },
      county: {
        countyName: desc.countyName,
        countyCode: addr.countyCode
      }
    }));
  };
  handleDelete(addr) {
    this.props.showConfirmation({
      content: '确认删除该地址吗？',
      onConfirm: () => this.deleteAddressForReal(addr)
    });
  };
  async deleteAddressForReal(addr) {
    try {
      loading.show();
      await axios.get('/user-address-delete?id=' + addr.id);
    } catch(e) {
      this.tip.show(e);
      console.error(e);
    } finally {
      loading.hide();
    }
    addressHandler.remove(addr.id);
    this.forceUpdate();
  }
  async handleSetAsDefault(addr) {
    try {
      const updated = {
        ...addr,
        defaultUnit: 'Y'
      };
      const queryString = '?' + qs.stringify(updated);
      loading.show();
      await axios.get('/user-address-modify' + queryString);
      addressHandler.update(updated);
      this.forceUpdate();
    } catch (e) {
      this.tip.show(e);
      console.error(e);
    } finally {
      loading.hide();
    }
  }
  render() {
    return (
      <div styleName="address-list">
        {
          addresses.map(addr => (
            <div styleName="address" key={addr.id}>
              <AddressCard addressInfo={addr}
                           onSelect={() => this.handleSelect(addr.id)}
                           onEdit={() => this.handleEdit(addr)}
                           onDelete={() => this.handleDelete(addr)}
                           onSetAsDefault={() => this.handleSetAsDefault(addr)}/>
            </div>
          ))
        }
        <StickyFooter height={118}>
          <Button style={styles.button} onClick={this.addAddress}>
            添加收货地址
          </Button>
        </StickyFooter>
        <Tip ref={tip => this.tip = tip}/>
      </div>
    );
  }
};

const styles = {
  button: {
    backgroundColor: '#12c287',
    color: 'white',
    marginTop: '.15rem'
  }
};

AddressList.propTypes = {};
AddressList.defaultProps = {};

export default AddressList;
