import React from 'react';
import PropTypes from 'prop-types';
import CSSModules from 'react-css-modules';
import styles from './product-list.scss';
import ProductItem from '../../components/ProductItem/ProductItem';
import { withRouter } from 'react-router-dom';
import qs from 'qs';

@withRouter
@CSSModules(styles)
class ProductList extends React.Component {
  goToProductDetail = ({activityType, activityId, openUrl}) => {
    if (activityType == 7) {
      location.href = openUrl;
    } else {
      this.props.history.push('/productDetail?' + qs.stringify({ activityId }));
    }
  };
  render() {
    const items = this.props.items;
    const lineAmt = Math.ceil(items.length / 2);
    return (
      <div styleName="product-list">
        {
          Array(lineAmt).fill("").map((d, i) => {
            if (2 * i + 1 <= items.length - 1) {
              return (
                <div styleName="line" key={i}>
                  <ProductItem info={items[2 * i]} onItemClick={this.goToProductDetail}/>
                  <ProductItem info={items[2 * i + 1]} onItemClick={this.goToProductDetail}/>
                </div>
              )
            }
            return (
              <div styleName="line" key={i}>
                <ProductItem info={items[2 * i]} onItemClick={this.goToProductDetail}/>
                <div></div>
              </div>
            );
          })
        }
      </div>
    );
  }
};

ProductList.propTypes = {};
ProductList.defaultProps = {};

export default ProductList;
