const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const common = require('./webpack.common');
const autoprefixer = require('autoprefixer');
const fs = require('fs');

module.exports = merge(common, {
  devtool: 'inline-source-map',
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    host: require('my-local-ip')(),
    disableHostCheck: true,
    port: 9000,
    open: true,
    hot: true,
    overlay: true,
    stats: 'errors-only',
    historyApiFallback: true,
    proxy: {
      "/appapi/za-plutuscat": {
        target: "https://mgw-daily.zhongan.com",
        // pathRewrite: {"^/https://mgw-daily.zhongan.com/appapi/za-plutuscat" : ""},
        secure: false,
        changeOrigin: true
      }
    }
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin()
  ],
  module: {
    rules: [
      {
        test: /\.(css|scss)$/,
        exclude: /node_modules/,
        use: [
          'style-loader?sourceMap',
          {
            loader: 'css-loader',
            options: {
              camelCase: true,
              modules: true,
              importLoaders: 1,
              localIdentName: '[path][name]__[local]--[hash:base64:5]'
            }
          },
          {
            loader: require.resolve('postcss-loader'),
            options: {
              // Necessary for external CSS imports to work
              // https://github.com/facebookincubator/create-react-app/issues/2677
              ident: 'postcss',
              sourceMap: true,
              plugins: () => [
                require('postcss-flexbugs-fixes'),
                autoprefixer({
                  remove: false
                }),
              ],
            },
          },
          'sass-loader?sourceMap'
        ]
      }
    ]
  }
});

