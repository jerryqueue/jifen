module.exports = {
  // sever account, address, port
  server: 'static:Static123@10.253.4.128:22',
  // deploy all files in the directory
  workspace: __dirname + '/dist',
  // ignore the matched files (glob pattern: https://github.com/isaacs/node-glob#glob-primer)
  // support array of glob pattern
  ignore: '**/*.map',
  // where the files are placed on the server
  deployTo: '/www/website/assets/subject/react/jifenmall'
};