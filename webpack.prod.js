const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const PurifyCSSPlugin = require('purifycss-webpack');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');
const glob = require('glob-all');
const common = require('./webpack.common');
// const WebpackFtpPlugin = require('webpack-ftp-plugin');
const DeployPlugin = require('deploy-kit/plugins/sftp-webpack-plugin');
const autoprefixer = require('autoprefixer');
const env = require('./env');

module.exports = merge(common, {
  devtool: 'source-map',
  output: {
    filename: '[name].[chunkhash].js',
    publicPath: env.publicPath
  },
  plugins: [
    new UglifyJSPlugin({
      compress: {
        warnings: false,
        drop_console: false,
        drop_debugger: true
      }
    }),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('production')
    }),
    new webpack.HashedModuleIdsPlugin(),
    new ExtractTextPlugin({
      filename: 'css/[name]-style-[contenthash:10].css',
      allChunks: true
    }),
    new PurifyCSSPlugin({
      // Give paths to parse for rules. These should be absolute!
      paths: glob.sync(path.join(__dirname, './*.html')).concat(
        glob.sync(path.join(__dirname, './src/**/*.js'))
      ),
      minimize: true,
      moduleExtensions: ['.js'], //An array of file extensions for determining used classes within node_modules
      purifyOptions: {
        whitelist: ['*purify*']
      }
    }),
    // new WebpackFtpPlugin({
    //   remoteRoot: '/www/website/assets/subject/react/jifenmall',
    //   localRoot: './dist',
    //   deployPath: './'
    // })
    // new DeployPlugin()
    // new FaviconsWebpackPlugin('./logo.svg')
  ],
  module: {
    rules: [
      {
        test: /\.(css|scss)$/,
        exclude: /node_modules/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader',
              options: {
                minimize: true,
                camelCase: true,
                modules: true,
                localIdentName: 'purify_[hash:base64:10]'
              }
            },
            {
              loader: require.resolve('postcss-loader'),
              options: {
                // Necessary for external CSS imports to work
                // https://github.com/facebookincubator/create-react-app/issues/2677
                ident: 'postcss',
                plugins: () => [
                  require('postcss-flexbugs-fixes'),
                  autoprefixer({
                    remove: false
                  }),
                ],
              },
            },
            'sass-loader'
          ]
        })
      }
    ]
  }
});

