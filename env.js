const baseUrlMap = {
  test: 'https://mgw-daily.zhongan.com/appapi/za-plutuscat',
  uat: 'https://gwbk-uat.zhongan.com/appapi/za-plutuscat',
  prod: 'https://gwbk.zhongan.com/appapi/za-plutuscat'
};
const oldBaseMap = {
  test: 'http://jf.test1.zhongan.com',
  uat: 'https://jf-uat.zhongan.com',
  prod: 'https://jf.zhongan.com'
};
const publicPathMap = {
  test: 'https://staticdaily.zhongan.com/website/assets/subject/react/jifenmall/',
  uat: 'https://staticdaily.zhongan.com/website/assets/subject/react/jifenmall/',
  prod: 'https://static.zhongan.com/website/assets/subject/react/jifenmall/'
};

const env = 'uat';

module.exports = {
  publicPath: process.env.NODE_ENV === 'production' ?
    publicPathMap[env] : '/',
  baseURL: process.env.NODE_ENV === 'production' ?
    baseUrlMap[env] : 'http://10.253.172.62:30997/',
  oldBase: oldBaseMap[env]
};